{-# LANGUAGE OverloadedStrings #-}

import Common
import Data.Aeson (encode, decode)
import qualified Data.ByteString.Lazy.Char8 as BS

testDec :: BS.ByteString -> IO ()
testDec = print . (decode :: BS.ByteString -> Maybe Hero)

main :: IO ()
main = do
    BS.putStrLn $ encode $ Hero "toto" "42"
    testDec "{\"heroName\":\"toto\", \"heroImage\":\"42\"}"
    testDec "{\"heroName\":\"toto\", \"heroImage\":42}"
    testDec "{\"heroName\":\"toto\"}"
    testDec "{\"heroName\":\"toto\", \"heroImage\":\"42\", \"x\":7}"


