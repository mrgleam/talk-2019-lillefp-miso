import Common
import Data.Aeson (decodeStrict)
import Data.Maybe (fromJust, fromMaybe)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String (ms)

main :: IO ()
main = do
    heroes <- xhrHeroes
    print heroes

xhrHeroes :: IO [Hero]
xhrHeroes = do
    let uri = ms "heroes"
    let request = Request GET uri Nothing [] False NoData
    response <- xhrByteString request
    pure $ fromMaybe [] $ decodeStrict $ fromJust $ contents response

