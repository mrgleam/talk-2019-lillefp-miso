import Common
import Data.Aeson (decodeStrict)
import Data.Maybe (fromJust, fromMaybe)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String (ms)

main :: IO ()
main = startApp App 
    { initialAction = NoOp
    , model = initModel
    , update = updateModel
    , view = homeView
    , events = defaultEvents
    , subs = [ ]
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel PopHeroes m = 
    if null (heroes_ m)
    then noEff m
    else noEff m { heroes_ = tail (heroes_ m) }
updateModel (SetHeroes heroes) m = noEff m { heroes_ = heroes }
updateModel FetchHeroes m = m <# (SetHeroes <$> xhrHeroes)

xhrHeroes :: IO [Hero]
xhrHeroes = do
    let uri = ms "heroes"
    let request = Request GET uri Nothing [] False NoData
    response <- xhrByteString request
    pure $ fromMaybe [] $ decodeStrict $ fromJust $ contents response

