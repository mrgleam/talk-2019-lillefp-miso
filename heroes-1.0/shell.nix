let

  pkgs = import ./nix/nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "heroes" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "heroes" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

