
data Rectangle a = Rectangle a a

main = putStrLn $ show $ Rectangle 4 2
-- main = print $ Rectangle 4 2

instance Show (Rectangle a) where
    show (Rectangle w h) = "Rectangle"

