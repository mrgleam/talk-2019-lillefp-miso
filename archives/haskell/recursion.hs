length' :: [Int] -> Int
length' [] = 0
length' (_:xs) = 1 + length' xs

length'' :: [Int] -> Int
length'' = auxFun 0 
    where auxFun n [] = n
          auxFun n (_:xs) = auxFun (n+1) xs

main = do
    print $ length' [1..4]
    print $ length'' [1..4]

