
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just $ sqrt x else Nothing

mul2 :: Double -> Double
mul2 x = x*2

main :: IO ()
main = do
    print $ fmap mul2 $ safeSqrt (-1)
    print $ mul2 <$> safeSqrt 441

