{-# LANGUAGE OverloadedStrings #-}

import Lucid
import qualified Data.ByteString.Lazy.Char8 as BS

myPage0 :: Html ()
myPage0 = div_ [] $ do
    p_ "my bookmarks:"
    ul_ $ do
        li_ $ a_ [href_ "https://www.haskell.org"] "haskell"
        li_ $ a_ [href_ "https://haskell-miso.org/"] "miso"

myPage :: Html ()
myPage = 
    ul_ $ do
        li_ "hello"
        li_ $ a_ [href_ "https://haskell-miso.org/"] "miso"

main = BS.putStrLn $ renderBS myPage 

