{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as BS
import GHC.Generics

data Info = Info 
    { message :: String
    } deriving (Eq, Generic, Show)

instance FromJSON Info
instance ToJSON Info

main = do
    BS.putStrLn $ encode $ Info { message = "toto" }
    print (decode "{\"message\":\"tata\"}" :: Maybe Info)
    print (decode "{\"message\":42}" :: Maybe Info)
    print (decode "{\"message\":\"7\",\"a\":7}" :: Maybe Info)

