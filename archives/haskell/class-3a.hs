data Color = Blue | White | Red

main = print Red

instance Show Color where
    show Blue  = "Blue"
    show White = "White"
    show Red   = "Red"


