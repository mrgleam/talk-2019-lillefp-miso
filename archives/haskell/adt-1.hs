
-- union type

data Color = Blue | White | Red

getCode :: Color -> String
getCode Blue  = "#0000FF"
getCode White = "#FFFFFF"
getCode Red   = "#FF0000"

main = putStrLn $ getCode Red

