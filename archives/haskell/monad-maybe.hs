
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just $ sqrt x else Nothing

safeMul2 :: Double -> Maybe Double
safeMul2 x = Just $ x*2

main :: IO ()
main = do
    print (safeSqrt 441 >>= safeMul2)
    print (safeMul2 882 >>= safeSqrt)
    print $ do
        s <- safeSqrt 441
        safeMul2 s

