let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "app" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "app" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

