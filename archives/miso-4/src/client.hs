{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import           Data.Aeson
import           Data.Maybe
import           GHC.Generics
import           JavaScript.Web.XMLHttpRequest
import           Miso
import qualified Miso.String as MS

data Info = Info 
    { commit :: Commit
    } deriving (Eq, Generic)

data Commit = Commit 
    { message :: MS.MisoString
    } deriving (Eq, Generic)

instance FromJSON Info
instance FromJSON Commit

data Model = Model 
    { infos_ :: [Info]
    } deriving (Eq)

viewModel :: Model -> View Action
viewModel m = div_ []
    [ h1_ [] [ text "miso-4" ]
    , ul_ [] (map fmtInfo $ infos_ m)
    ]
    where fmtInfo i = li_ [] [text $ MS.ms $ message $ commit i]

data Action
  = NoOp
  | FetchApi
  | SetInfo [Info]
  deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel FetchApi m = m <# do SetInfo <$> xhrApi
updateModel (SetInfo infos) m = noEff (Model infos)
-- updateModel (SetInfo infos) m = noEff m { infos_ = infos }

xhrApi :: IO [Info]
xhrApi = fromMaybe [] . decodeStrict . fromJust . contents <$> 
    xhrByteString Request 
        { reqMethod = GET
        , reqURI = "https://api.github.com/repos/dmjio/miso/commits"
        , reqLogin = Nothing
        , reqHeaders = []
        , reqWithCredentials = False
        , reqData = NoData
        }

main :: IO ()
main = startApp App 
    { model = Model []
    , initialAction = FetchApi
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

