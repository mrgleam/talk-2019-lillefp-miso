{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Common
import Data.Proxy 
import qualified Data.Text as T
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.API
import Servant.Client
import Servant

message :: ClientM Message
double :: Int -> ClientM Int
hello :: Maybe T.Text -> ClientM T.Text
message :<|> double :<|> hello = client api

queries1 :: ClientM (Message, Int, T.Text)
queries1 = do
    m <- message
    d <- double 21
    h <- hello Nothing
    return (m, d, h)

queries2 :: ClientM (Message, Int, T.Text)
queries2 = (,,) <$> message <*> double 21 <*> hello Nothing

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    let env = mkClientEnv manager (BaseUrl Http "localhost" 3000 "")
    runClientM message env >>= print
    runClientM (double 21) env >>= print
    runClientM (hello Nothing) env >>= print
    runClientM queries1 env >>= print
    runClientM queries2 env >>= print

