{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Common where

import Data.Aeson
import qualified Data.Text as T
import GHC.Generics
import Servant

data Message = Message 
    { msg :: T.Text 
    } deriving (Generic, Show)

instance ToJSON Message
instance FromJSON Message

type Api 
    =    "message" :> Get '[JSON] Message
    :<|> "double" :> Capture "nb" Int :> Get '[JSON] Int
    :<|> "hello" :> QueryParam "name" T.Text :> Get '[PlainText] T.Text

api :: Proxy Api
api = Proxy

