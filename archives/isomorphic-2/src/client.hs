{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import Data.Proxy
import Miso
import Miso.String
import Servant.API
import Servant.Links
import qualified Network.URI as Network

data Model = Model 
    { uri_ :: Network.URI
    , counter_ :: Int
    } deriving (Eq)

initialModel :: Network.URI -> Model
initialModel u = Model u 0

data Action
    = NoOp
    | ChangeURI Network.URI
    | HandleURI Network.URI
    | SubOne
    | AddOne
    deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel (HandleURI u) m = noEff m { uri_ = u } 
updateModel (ChangeURI u) m = m <# (pushURI u >> return NoOp)
updateModel SubOne m = noEff m { counter_ = counter_ m - 1 }
updateModel AddOne m = noEff m { counter_ = counter_ m + 1 }  

type ClientRoutes = HomeRoute :<|> CounterRoute

type HomeRoute = View Action

type CounterRoute = "counter" :> View Action

viewModel :: Model -> View Action
viewModel m = case runRoute (Proxy @ClientRoutes) clientRoutes uri_ m of
    Left _ -> text "not found"
    Right v -> v

clientRoutes :: (Model -> View Action) :<|> (Model -> View Action)
clientRoutes = homeView :<|> counterView

homeView :: Model -> View Action
homeView _ = div_ []
    [ h1_ [] [ text "home" ]
    , button_ [ onClick $ ChangeURI counterLink ] [text "counter"]
    ]

counterView :: Model -> View Action
counterView (Model _ c) = div_ []
    [ h1_ [] [ text "counter" ]
    , button_ [onClick $ ChangeURI homeLink] [text "home"]
    , p_ []
        [ button_ [ onClick SubOne ] [ text "-" ]
        , text $ ms $ show c
        , button_ [ onClick AddOne ] [ text "+" ]
        ]
    ]

homeLink :: Network.URI
homeLink = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @HomeRoute)

counterLink :: Network.URI
counterLink = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @CounterRoute)

main :: IO ()
main = startApp App
    { initialAction = NoOp
    , model = initialModel homeLink
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = [ uriSub HandleURI ]
    , mountPoint = Nothing
    }

