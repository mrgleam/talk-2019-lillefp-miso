{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import Common
import qualified Lucid as L
import Miso 
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.RequestLogger (logStdout)
import Servant

main :: IO ()
main = run 3000 $ logStdout $ serve (Proxy @ServerApi) server

newtype HtmlPage a = HtmlPage a
    deriving (Eq, Show)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.with (L.script_ mempty)
                 [L.src_ "/static/all.js", L.async_ mempty, L.defer_ mempty]
        L.body_ (L.toHtml x)

type ServerApi
    =    "static" :> Raw 
    :<|> ToServerRoutes ClientRoutes HtmlPage Action

server :: Server ServerApi
server
    =    serveDirectoryFileServer "static"
    :<|> (homeHandler :<|> counterHandler)

homeHandler :: Handler (HtmlPage (View Action))
homeHandler = return $ HtmlPage $ viewModel $ initialModel homeLink

counterHandler :: Handler (HtmlPage (View Action))
counterHandler = return $ HtmlPage $ viewModel $ initialModel counterLink

