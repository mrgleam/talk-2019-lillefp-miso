import Common
import Miso

main :: IO ()
main = miso $ \ currentUri -> App 
    { initialAction = NoOp
    , model = initialModel currentUri
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = [ uriSub HandleURI ]
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel (HandleURI u) m = m { uri_ = u } <# return NoOp
updateModel (ChangeURI u) m = m <# (pushURI u >> return NoOp)
updateModel SubOne m = noEff m { counter_ = counter_ m - 1 }
updateModel AddOne m = noEff m { counter_ = counter_ m + 1 }  

