{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.RequestLogger (logStdout)
import Servant

main :: IO ()
main = run 3000 $ logStdout $ serve (Proxy @ServerApi) server

type ServerApi = Raw 

server :: Server ServerApi
server
    =    serveDirectoryFileServer "static"


